import urllib.request
import json
from bs4 import BeautifulSoup


def sanitize_name(n):
    return n.replace("'", '`').replace("A`", "à").replace("E`", "è").replace("O`", "ò").replace("I`", "ì").replace("U`", "ù").split("`")[0]


class StationFetcher:
    id_names = {}
    name_id = {}
    id_prm = {}

    def __init__(self):
        page = BeautifulSoup(urllib.request.urlopen(
            "https://iechub.rfi.it/ArriviPartenze").read(), features="lxml")
        for station in page.find(id="ElencoLocalita").find_all("option"):
            self.id_names[int(station["value"])] = station.string
            self.name_id[station.string] = int(station["value"])
        try:
            f = open("prm_id_cache.json")
            self.id_prm = json.load(f)
            f.close()
        except OSError:
            for tab_id in self.id_names.keys():
                self.id_prm[tab_id] = self.get_station_prm_id_from_name(
                    self.id_names[tab_id])
                f = open("prm_id_cache.json", 'w')
                json.dump(self.id_prm, f)
                f.close()

    # def get_station_with_id(self, id):
    #    return self.id_names[int(id)] if int(id) in self.id_names else None

    def get_station_id(self, name):
        uc_name = name.upper().strip()
        return self.id_prm[self.name_id[uc_name]] if uc_name in self.name_id else None

    def get_station_with_id_board(self, id):
        return self.id_names[int(id)] if int(id) in self.id_names else None

    def get_station_id_board(self, name):
        uc_name = name.upper().strip()
        return self.name_id[uc_name] if uc_name in self.name_id else None

    def get_station_prm_id_from_name(self, name):
        api_resp = urllib.request.urlopen(urllib.request.Request(
            url="https://prm.rfi.it/qo_prm/WebService.asmx/GetCompletionList", method="POST", headers={"Content-Type": "application/json"}, data=bytes(json.dumps({"prefixText": sanitize_name(name), "count": 40, "contextKey": "false Tutte le Regioni"}), "utf-8"))).read()
        prm_id = json.loads(
            json.loads(api_resp)["d"][0])["Second"]
        return prm_id


def parse_stops(cell):
    result = []
    for i, e in enumerate(cell.children):
        new_index = (i+1) % 4
        if new_index == 2:
            result.append(e.name == "img")
        elif new_index == 3:
            pieces = e.string.strip().split("(")
            result[-1] = (" (".join(pieces[:-1]).strip(), pieces[-1].replace('.', ':')
                          [:-1], result[-1])
    return result


def parse_destination(cell):
    text = cell.text.strip().split(" (")
    return (" (".join(text[:-1]).strip(), text[-1].replace('.', ':')
            [:-1], cell.find("img") is not None)


def parse_row(row_parts, station_name, prm):
    return {
        "number": (row_parts[1].img["src"], row_parts[1].text.strip()),
        "platform": row_parts[3].text.strip(),
        "carrier": row_parts[5].text.strip(),
        "stops": parse_stops(row_parts[6]) + [(station_name, row_parts[0].string.strip().replace('.', ':'), prm)] + parse_stops(row_parts[7]) + [parse_destination(row_parts[2])],
        "regularity": row_parts[8].text.strip(),
        "notices": row_parts[9].text.strip()
    }


def get_timetable_for_station(id):
    page = BeautifulSoup(urllib.request.urlopen(
        "https://prm.rfi.it/qo_prm/QO_Stampa.aspx?Tipo=P&id="+str(id)+"&lin=it&dalle=00.00&alle=23.59").read(), features="lxml")
    try:
        timetable = page.find(attrs={"class":  "QOtab"}).tbody
        title = page.find(attrs={"class":  "QOtitolo"})
        return [parse_row(entry.find_all("td"), title.h3.string.replace(
                "Stazione di ", ''), title.img["src"] == "images/ico_pmr_big.gif") for entry in timetable.find_all("tr")]
    except:
        return []


# get_timetable_for_station(1)
