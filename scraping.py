from stations import *
from json import dumps
from urllib.error import HTTPError


def scrape(stations):
    trains = {}
    no_of_s = len(stations)
    for i, station in enumerate(stations):
        print(i/no_of_s*100, "%, ", i+1, "/", no_of_s)
        if station == "":
            continue
        nt = 0
        try:
            for train in get_timetable_for_station(station):
                train_id = train["carrier"] + '-' + train["number"][1]
                if train_id in trains:
                    continue
                trains[train_id] = train
                nt = nt+1
            f = open("trains.json", 'w')
            f.write(dumps(trains))
            f.close()
        except HTTPError:
            print("Failed station ", station)
        print("Station", station, "added", nt, "trains!")


if __name__ == "__main__":
    sf = StationFetcher()
    scrape(sf.id_prm.values())
