import json
from scraping import scrape
from stations import StationFetcher


def get_station_id(sf, name):
    dumb = sf.get_station_id_board(name.upper())
    if dumb is None:
        return sf.get_station_prm_id_from_name(name)
    else:
        return sf.id_prm[str(dumb)]


stations = []
old_data = json.load(open("trains.json"))
for train in old_data.values():
    start = train["stops"][0][0].upper()
    end = train["stops"][-1][0].upper()
    if start not in stations:
        stations.append(start)
    # if end not in stations:
    #    stations.append(end)
rfi_sf = StationFetcher()
stations_with_id = [(i, get_station_id(rfi_sf, i)) for i in stations]
f = open("trains.json.old", 'w')
json.dump(old_data, f)
f.close()
scrape([i[1] for i in stations_with_id])
